import com.intellij.ide.plugins.IdeaPluginDescriptor;
import com.intellij.ide.plugins.PluginManagerCore;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.lang.Language;
import com.intellij.openapi.extensions.PluginId;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.cert.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class StatsCollector {
    private static StatsCollector instance;

    /**
     * How long to wait before sending an update.
     */
    private static final long UPDATE_TIMER = 10;

    // Is this bad? I have no idea. This is for the xps synchronization
    private final Object xps_lock = new Object();

    private Hashtable<String, Integer> xps;
    private ScheduledFuture<?> updateTimer;
    private ScheduledExecutorService executor;
    private UpdateTask updateTask;

    private String apiKey;

    private List<StatusBarIcon> statusBarIcons = new ArrayList<StatusBarIcon>();

    public String pluginVersion;

    public static StatsCollector getInstance() {
        synchronized (StatsCollector.class) {
            if (instance == null) {
                PropertiesComponent propertiesComponent = PropertiesComponent.getInstance();
                String apiKey = propertiesComponent.getValue(SettingsForm.API_KEY_NAME);
                String apiURL = propertiesComponent.getValue(SettingsForm.API_URL_NAME);

                IdeaPluginDescriptor plugin = PluginManagerCore.getPlugin(PluginId.getId("net.codestats.plugin.atom.intellij"));
                String pluginVersion;
                if (plugin != null) {
                    pluginVersion = plugin.getVersion();
                }
                else {
                    pluginVersion = "unknown";
                }


                instance = new StatsCollector(apiKey, apiURL, pluginVersion);
            }

            return instance;
        }
    }

    private StatsCollector(String apiKey, String apiURL, String pluginVersion) {
        this.apiKey = apiKey;
        this.pluginVersion = pluginVersion;

        executor = Executors.newScheduledThreadPool(1);
        xps = new Hashtable<>();
        updateTask = new UpdateTask();

        updateTask.setConfig(apiURL, apiKey);
        updateTask.setSSLContext(installLECACert());
        updateTask.setVersion(pluginVersion);
        updateTask.setXpsLock(xps_lock);

        CodeStatsLogger.debug("Starting up code-stats-intellij/" + pluginVersion);
    }

    public void registerStatusBarIcon(StatusBarIcon statusBarIcon) {
        statusBarIcons.add(statusBarIcon);
        updateTask.setStatusBarIcons(statusBarIcons);
    }

    public void unRegisterStatusBarIcon(StatusBarIcon statusBarIcon) {
        statusBarIcons.remove(statusBarIcon);
        updateTask.setStatusBarIcons(statusBarIcons);
    }

    public void handleKeyEvent(Language language) {
        if (apiKey == null) {
            // Don't collect data without an API key
            return;
        }

        final String languageName = language.getDisplayName();

        synchronized (xps_lock) {
            if (xps.containsKey(languageName)) {
                xps.put(languageName, xps.get(languageName) + 1);
            } else {
                xps.put(languageName, 1);
            }
        }

        // If timer is already running, cancel it to prevent updates when typing
        if (updateTimer != null && !updateTimer.isCancelled()) {
            updateTimer.cancel(false);
        }

        updateTask.setXps(xps);
        updateTimer = executor.schedule(updateTask, UPDATE_TIMER, TimeUnit.SECONDS);
    }

    public void setApiConfig(final String apiURL, final String apiKey) {
        this.apiKey = apiKey;
        updateTask.setConfig(apiURL, apiKey);

        CodeStatsLogger.debug("Updated code-stats-intellij configuration.");
    }

    // Code::Stats is using a Let's Encrypt certificate which Java does not trust by default.
    // We need to add the bundled LE root CA certificate to the trust store before we can send any calls to the server.
    // See: http://stackoverflow.com/a/34111150

    // Both ISRG root X1 and DST root X3 are added, because we might not know which one has signed our LE certificate in
    // the future.

    // Returns the SSL context to use in HTTPS requests
    private SSLContext installLECACert() {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            Path ksPath = Paths.get(System.getProperty("java.home"),
                    "lib", "security", "cacerts");

            try {
                keyStore.load(Files.newInputStream(ksPath), "changeit".toCharArray());
            } catch (IOException e) {
                CodeStatsLogger.warn("Unable to open keystore, IOException occurred: " + e.getMessage());
            }

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            try (InputStream caInput = new BufferedInputStream(
                    StatsCollector.class.getResourceAsStream("dstrootx3real.der"))) {
                Certificate crt = cf.generateCertificate(caInput);
                CodeStatsLogger.debug("Added Cert for " + ((X509Certificate) crt)
                        .getSubjectDN());

                keyStore.setCertificateEntry("DST root CA X3", crt);
            }

            try (InputStream caInput = new BufferedInputStream(
                    StatsCollector.class.getResourceAsStream("isrgrootx1.der"))) {
                Certificate crt = cf.generateCertificate(caInput);
                CodeStatsLogger.debug("Added Cert for " + ((X509Certificate) crt)
                        .getSubjectDN());

                keyStore.setCertificateEntry("ISRG root X1", crt);
            }

            CodeStatsLogger.debug("Truststore now trusting: ");
            PKIXParameters params = new PKIXParameters(keyStore);
            params.getTrustAnchors().stream()
                    .map(TrustAnchor::getTrustedCert)
                    .map(X509Certificate::getSubjectDN)
                    .forEach(principal -> CodeStatsLogger.debug(principal.getName()));

            TrustManagerFactory tmf = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(keyStore);
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            return sslContext;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
