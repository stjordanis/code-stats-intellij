import com.intellij.lang.Language;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.util.PsiUtilBase;
import org.jetbrains.annotations.NotNull;

public class GenericHandler {
    final private StatsCollector statsCollector;

    public GenericHandler() {
        statsCollector = StatsCollector.getInstance();
    }

    public void execute(@NotNull Editor editor, Runnable originalHandler) {
        try {
            final Project project = editor.getProject();
            if (project != null) {
                final Language language = PsiUtilBase.getLanguageInEditor(editor, project);
                if (language != null) {
                    statsCollector.handleKeyEvent(language);
                }
            }
        } finally {
            // Ensure original handler is called no matter what errors are thrown, to prevent typing from being lost
            originalHandler.run();
        }
    }

    public int execute(int caretsToIgnore, @NotNull Editor editor, Runnable originalHandler) {
        if (caretsToIgnore > 0) {
            --caretsToIgnore;

            // Ensure we run original handler even when ignoring carets
            originalHandler.run();
            return caretsToIgnore;
        }

        final int caretCount = editor.getCaretModel().getCaretCount();
        if (caretCount > 1) {
            caretsToIgnore = caretCount - 1;
        }

        this.execute(editor, originalHandler);
        return caretsToIgnore;
    }
}
