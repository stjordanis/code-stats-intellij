import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.StatusBarWidget;
import com.intellij.openapi.wm.StatusBarWidgetFactory;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

public class StatusBarIconProvider implements StatusBarWidgetFactory {
    @NotNull
    @Override
    public String getId() {
        return "code-stats-intellij-status-bar-widget";
    }

    @Nls
    @NotNull
    @Override
    public String getDisplayName() {
        return "Code::Stats";
    }

    @Override
    public boolean isAvailable(@NotNull Project project) {
        return true;
    }

    @NotNull
    @Override
    public StatusBarWidget createWidget(@NotNull Project project) {
        return new StatusBarIcon(project);
    }

    @Override
    public void disposeWidget(@NotNull StatusBarWidget widget) {}

    @Override
    public boolean canBeEnabledOn(@NotNull StatusBar statusBar) {
        return true;
    }
}
