import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.StatusBarWidget;
import com.intellij.util.Consumer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.event.MouseEvent;

public class StatusBarIcon implements StatusBarWidget {
    public static final String STATUS_BAR_ID_PREFIX = "code-stats-intellij-status-bar-icon-";

    private static StatsCollector statsCollector;

    private static class StatusBarPresentation implements TextPresentation {

        private String text = "C::S";
        private String tooltipText = "";

        @NotNull
        @Override
        public String getText() {
            return text;
        }

        @Override
        public float getAlignment() {
            return 0;
        }

        @Nullable
        @Override
        public String getTooltipText() {
            return tooltipText;
        }

        @Nullable
        @Override
        public Consumer<MouseEvent> getClickConsumer() {
            return null;
        }

        public void setText(String text) {
            this.text = text;
        }

        public void setToolTipText(String toolTipText) {
            this.tooltipText = toolTipText;
        }
    }

    private String ID;
    private StatusBarPresentation statusBarPresentation;
    private StatusBar statusBar;

    StatusBarIcon(@NotNull Project project) {
        this.ID = STATUS_BAR_ID_PREFIX + project.getName();
        statusBarPresentation = new StatusBarPresentation();

        statsCollector = StatsCollector.getInstance();
    }

    @NotNull
    @Override
    public String ID() {
        return ID;
    }

    @Nullable
    @Override
    public WidgetPresentation getPresentation() {
        return statusBarPresentation;
    }

    @Override
    public void install(@NotNull StatusBar statusBar) {
        this.statusBar = statusBar;
        updateStatusBars();
        statsCollector.registerStatusBarIcon(this);
    }

    @Override
    public void dispose() {
        statusBarPresentation = null;
        statsCollector.unRegisterStatusBarIcon(this);
    }

    public void setUpdating() {
        if (statusBarPresentation != null) {
            statusBarPresentation.setText("C::S…");
            statusBarPresentation.setToolTipText("Updating…");
            updateStatusBars();
        }
    }

    public void setError(final String error) {
        if (statusBarPresentation != null) {
            statusBarPresentation.setText("C::S ERR!");
            statusBarPresentation.setToolTipText("An error occurred:\n" + error);
            updateStatusBars();
        }
    }

    public void clear() {
        if (statusBarPresentation != null) {
            statusBarPresentation.setText("C::S");
            statusBarPresentation.setToolTipText(null);
            updateStatusBars();
        }
    }

    private void updateStatusBars() {
        // Trigger repaint on this icon's statusbar
        statusBar.updateWidget(ID);
    }
}
