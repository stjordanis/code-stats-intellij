import com.intellij.openapi.editor.actionSystem.EditorActionHandler;

public class BackSpaceHandler extends GenericEditorActionHandler {
    public BackSpaceHandler(EditorActionHandler editorActionHandler) {
        super(editorActionHandler);
    }
}
